from rest_framework import serializers

from prestamos.models import *


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['name', 'phone', 'address','birthday']

class LoanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Loan
        fields = ['id', 'client', 'amount', 'saldo', 'date', 'date_update']

    def to_representation(self, instance):
        self.fields['client'] = ClientSerializer(read_only=True)
        return super().to_representation(instance)
