from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import JsonResponse
from prestamos.api.serializer import *
from prestamos.models import *
from django.db.models import Sum


class LoanViewSet(viewsets.ModelViewSet):
    queryset = Loan.objects.all()
    serializer_class = LoanSerializer


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

class LoanPruebaInteres(viewsets.ModelViewSet):
    serializer_class = LoanSerializer
    queryset = Loan.objects.annotate(total_loan = Sum('amount'))
    # queryset = Loan.objects.all)
