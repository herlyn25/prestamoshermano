from django.urls import path
from rest_framework import routers

from prestamos import views
from prestamos.api.apiviews import *

router = routers.DefaultRouter()
router.register('loans', LoanViewSet)
router.register('clients', ClientViewSet)
router.register('pruebaloan', LoanPruebaInteres)
urlpatterns = [ 
    path('lista/', views.ListPrestamosListView, name='lista'),
    path('listapdf/', views.ListPrestamosPDF.as_view(), name='listapdf'),
    path('edad/',views.MostrarEdad,name='edad'),
    path('listaacum', views.mostrardatoadicional,name='listaacum')
]