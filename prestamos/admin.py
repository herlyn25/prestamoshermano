from django.contrib import admin
from prestamos.models import *


# Register your models here.
@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'birthday']
    icon_name = 'person'


@admin.register(Loan)
class LoanAdmin(admin.ModelAdmin):
    list_display = ['client', 'amount', 'saldo', 'date', 'date_update']
    icon_name = 'monetization_on'


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = ['client', 'loan', 'amount']
    icon_name = 'payment'

