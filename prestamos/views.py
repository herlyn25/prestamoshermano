from datetime import datetime
from django.http import HttpResponse
from django.views import View
from django.views.generic import ListView
from django.shortcuts import render
from django.db.models.aggregates import Sum
from django.db.models import *
from prestamos import utils
from prestamos.models import Loan

def ListPrestamosListView(request):
    model = Loan
    queryset = Loan.objects.all()
    template_name = 'prestamolist.html'
    context = {'prestamos':queryset}
    return render(request,template_name, context=context)

class ListPrestamosPDF(View):
    def get(self,*args,**kwargs):
        loan = Loan.objects.all()
        data = { 'prestamos': loan}
        pdf = utils.render_to_pdf('prestamolist.html', data)
        return HttpResponse(pdf, content_type='application/pdf')

def diff_month(d1, d2):
    return (d1.year - d2.year) * 12 + d1.month - d2.month

def MostrarEdad(request):
    template_name = 'calcularedad.html'
    inicio ='25/07/1988'
    final = '05/01/2023'

    F_inicio = datetime.strptime(inicio, "%d/%m/%Y")
    F_final = datetime.strptime(final, "%d/%m/%Y")
    edad = diff_month(F_final,F_inicio)
    context = {'edad':str(edad)}
    return render(request,template_name,context=context)

def mostrardatoadicional(request):
    template_name='prestamolistagrupado.html'
    query = Loan.objects.values('client','client__name').annotate(intereses = Sum('amount'))
    print(query)
    context = {'datos': query}
    return render(request, template_name, context=context)