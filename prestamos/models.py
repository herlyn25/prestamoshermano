from django.db import models
from django.db.models.signals import post_save


# Create your models here.

class Client(models.Model):
    name = models.CharField(max_length=100)
    phone = models.IntegerField()
    address = models.CharField(max_length=100)
    state = models.BooleanField(default=True)
    birthday = models.DateField()

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'

    def __str__(self):
        return self.name


class Loan(models.Model):
    client = models.ForeignKey(Client, blank=True, null=True, on_delete=models.DO_NOTHING)
    amount = models.FloatField()
    date = models.DateField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)
    saldo = models.FloatField(default=0.0)

    class Meta:
        verbose_name = 'Prestamo'
        verbose_name_plural = 'Prestamos'

    def save(self, force_update=False):
        self.saldo = self.amount * 1.2
        super(Loan, self).save(force_update=force_update)

    def __str__(self):
        return f'Prestamo # {self.id}'


class Payment(models.Model):
    client = models.ForeignKey(Client, on_delete= models.DO_NOTHING)
    loan = models.ForeignKey(Loan,on_delete= models.DO_NOTHING)
    amount = models.FloatField()

    class Meta:
        verbose_name = 'Pago'
        verbose_name_plural = 'Pagos'


def update_balance_loan(sender, instance, **kwargs):
    prestamo_id = instance.loan.id
    if instance.loan.saldo > 0.0 and instance.amount < instance.loan.saldo:
        saldo =instance.loan.saldo-instance.amount
        prestamo = Loan.objects.filter(id=prestamo_id).update(saldo=saldo)
    else:
        print('Digite valor valido')


post_save.connect(update_balance_loan, sender= Payment)





